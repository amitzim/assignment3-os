#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "defs.h"
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
  char *s, *last;
  int i, off;
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
  #ifndef NONE
  int psyc_pages = 0;  
  int swapped_pages = 0;  
  int q_head = 0;
  int avl_slot = 0;
  int number_page_fault = 0;
  struct page_to_offset pages_array_in_swapfile[16];
  struct page_to_offset pages_array_in_physical[16];
  #endif
  begin_op();

  if((ip = namei(path)) == 0){
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
    goto bad;
  if(elf.magic != ELF_MAGIC)
    goto bad;

  if((pgdir = setupkvm()) == 0)
    goto bad;

#ifndef NONE

  avl_slot = curproc->avl_slot;
  number_page_fault = curproc->number_page_fault;
  psyc_pages = curproc->psyc_pages;  
  swapped_pages = curproc->swapped_pages;  
  q_head = curproc->q_head;
  for (int i = 0; i < 16 ; i++){
    memmove(&pages_array_in_swapfile[i], &curproc->pages_array_in_swapfile[i], sizeof(struct page_to_offset));
    memmove(&pages_array_in_physical[i], &curproc->pages_array_in_physical[i], sizeof(struct page_to_offset));
  }
  
  memset(&curproc->pages_array_in_swapfile, 0xffffffff, sizeof(curproc->pages_array_in_swapfile));
  memset(&curproc->pages_array_in_physical, 0xffffffff, sizeof(curproc->pages_array_in_physical));
  curproc->psyc_pages = 0;
  curproc->swapped_pages = 0;
  curproc->q_head = 0;
  curproc->avl_slot = 0;
  curproc->number_page_fault = 0;
  #endif
  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
    if(ph.vaddr + ph.memsz < ph.vaddr)
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(ph.vaddr % PGSIZE != 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
  end_op();
  ip = 0;

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;

  ustack[0] = 0xffffffff;  // fake return PC
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
    if(*s == '/')
      last = s+1;
  
  safestrcpy(curproc->name, last, sizeof(curproc->name));
	
  // Commit to the user image.
  oldpgdir = curproc->pgdir;
  curproc->pgdir = pgdir;
  curproc->sz = sz;
  curproc->tf->eip = elf.entry;  // main
  curproc->tf->esp = sp;
  
  removeSwapFile(curproc);
  createSwapFile(curproc);
  switchuvm(curproc);
  freevm(oldpgdir);
  // cprintf("exec: name: %s - before returning\n", curproc->name);
  return 0;

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
    end_op();
  }
  cprintf("exec: in bad label\n");
  #ifndef NONE
  for (int j = 0; j < 16 ; j++){
            // dst                                  src
    memmove(&curproc->pages_array_in_swapfile[j], &pages_array_in_swapfile[j],  sizeof(struct page_to_offset));
    memmove(&curproc->pages_array_in_physical[j], &pages_array_in_physical[j],  sizeof(struct page_to_offset));
  }
  curproc->q_head = q_head;
  curproc->number_page_fault = number_page_fault;
  curproc->swapped_pages = swapped_pages;
  curproc->psyc_pages = psyc_pages;
  curproc->avl_slot = avl_slot;
#endif
  return -1;
}


