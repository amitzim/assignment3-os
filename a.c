#include "types.h"
#include "stat.h"
#include "user.h"

#define PGSIZE	4096
#define NUM		4
#define ARRAY_SIZE		20
#define NFUA_MAX		14

void sanity1(){
	printf(1,"sanity1: START\n");
	char *x = (char *)malloc(NUM * PGSIZE);
	char *y = (char *)malloc(NUM * PGSIZE);
	char *z = (char *)malloc(NUM * PGSIZE);
	memset(x, 0x32, NUM * PGSIZE - 1);
	memset(y, 0x31, NUM * PGSIZE - 1);
	memset(z, 0x30, NUM * PGSIZE - 1);
	printf(1, "a: x = %s\n", x);
	printf(1, "a: y = %s\n", y);
	printf(1, "a: z = %s\n", z);
	printf(1, "a: x = %s\n", x);
	printf(1, "a: y = %s\n", y);
	printf(1, "a: z = %s\n", z);
	free(x);
	free(y);
	free(z);
	printf(1,"sanity1: END\n");
}

void scfifo_test(){
	printf(1,"scfifo_test: START\n");
	char *arr[ARRAY_SIZE] = {0};
	int i = 0;
	for (i = 0; i < ARRAY_SIZE; ++i){
		arr[i] = (char *)malloc(PGSIZE);
		// memset(arr[i], 0x30 + i, PGSIZE - 1);
		memset(arr[i], 0, PGSIZE);
		*arr[i] = 0x30 + i;
	}
	// printf(1, "%x\n", arr[5][1]);
	for(i = ARRAY_SIZE - 1 ; i >= 0 ; i--){
		printf(1, "%s\n", arr[i]);
	}
	printf(1,"scfifo_test: END\n");
}

void nfua(){
	printf(1, "nfua: START\n");
	char *arr[NFUA_MAX] = {0};
	int i = 0;
	for (i = 0 ; i < NFUA_MAX ; i++){
		arr[i] = (char *)malloc(PGSIZE);
		memset(arr[i], 0, PGSIZE - 1);
		// memset(arr[i], i, 1);
		*arr[i] = i;
	}
	int j = 0;
	for(j = 0 ; j < 640 ; j++){
		for(i = 0 ; i < NFUA_MAX - 1 ; i++){
			printf(1,"%s ", arr[i]);
		}
	}
	printf(1, "nfua: before page faul\n");
	printf(1,"%d", arr[NFUA_MAX - 1]);
	printf(1, "nfua: after page faul\n");

	printf(1, "nfua: END\n");
}

int
main(int argc, char *argv[]){
	sanity1();
	scfifo_test();
	nfua();
	exit();
}
