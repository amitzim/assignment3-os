#include "param.h"
#include "types.h"
#include "defs.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "elf.h"

#define PHYSICAl_ARRAY	0
#define SWAPPED_ARRAY	1
#define FIRST 			0

extern char data[];  // defined by kernel.ld
pde_t *kpgdir;  // for use in scheduler()
void page_out();

int count_ones(uint counter){
	int res = counter & 1;
	while(counter != 0){
		if(((counter >>= 1) & 1) == 1)
			res++;
		}
	return res;
}


// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
  struct cpu *c;

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpuid()];
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
  lgdt(c->gdt, sizeof(c->gdt));
}

// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
  pde_t *pde;
  pte_t *pgtab;
  // Takes 10 MSB's and checks if the entry exists
  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
      return 0;
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
}

// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    // cprintf("mappages: pte: %p\n", PTE_ADDR(*pte));
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}

// There is one page table per process, plus one that's used when
// a CPU is not running any process (kpgdir). The kernel uses the
// current process's page table during system calls and interrupts;
// page protection bits prevent user code from using the kernel's
// mappings.
//
// setupkvm() and exec() set up every page table like this:
//
//   0..KERNBASE: user memory (text+data+stack+heap), mapped to
//                phys memory allocated by the kernel
//   KERNBASE..KERNBASE+EXTMEM: mapped to 0..EXTMEM (for I/O space)
//   KERNBASE+EXTMEM..data: mapped to EXTMEM..V2P(data)
//                for the kernel's instructions and r/o data
//   data..KERNBASE+PHYSTOP: mapped to V2P(data)..PHYSTOP,
//                                  rw data + free physical memory
//   0xfe000000..0: mapped direct (devices such as ioapic)
//
// The kernel allocates physical memory for its heap and for user memory
// between V2P(end) and the end of physical memory (PHYSTOP)
// (directly addressable from end..P2V(PHYSTOP)).

// This table defines the kernel's mappings, which are present in
// every process's page table.
static struct kmap {
  void *virt;
  uint phys_start;
  uint phys_end;
  int perm;
} kmap[] = {
 { (void*)KERNBASE, 0,             EXTMEM,    PTE_W}, // I/O space
 { (void*)KERNLINK, V2P(KERNLINK), V2P(data), 0},     // kern text+rodata
 { (void*)data,     V2P(data),     PHYSTOP,   PTE_W}, // kern data+memory
 { (void*)DEVSPACE, DEVSPACE,      0,         PTE_W}, // more devices
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (P2V(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
                (uint)k->phys_start, k->perm) < 0) {
      freevm(pgdir);
      return 0;
    }
  return pgdir;
}

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
  kpgdir = setupkvm();
  switchkvm();
}

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
  lcr3(V2P(kpgdir));   // switch to the kernel page table
}

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
  if(p == 0)
    panic("switchuvm: no process");
  if(p->kstack == 0)
    panic("switchuvm: no kstack");
  if(p->pgdir == 0)
    panic("switchuvm: no pgdir");

  pushcli();
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
                                sizeof(mycpu()->ts)-1, 0);
  mycpu()->gdt[SEG_TSS].s = 0;
  mycpu()->ts.ss0 = SEG_KDATA << 3;
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
  // setting IOPL=0 in eflags *and* iomb beyond the tss segment limit
  // forbids I/O instructions (e.g., inb and outb) from user space
  mycpu()->ts.iomb = (ushort) 0xFFFF;
  ltr(SEG_TSS << 3);
  lcr3(V2P(p->pgdir));  // switch to process's address space
  popcli();
}

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
  char *mem;

  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
}

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
      panic("loaduvm: address should exist");
    pa = PTE_ADDR(*pte);
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
}


// adds entry to array 
// flag = 0 =>pages_array_in_physical ;; flag = 1 =>pages_array_in_swapfile
// advances lower entries by one
// void enqueue(struct proc *p, uint va){
//     int num_pages = p->psyc_pages;
//     p->pages_array_in_physical[num_pages].page_address = va;
//     if(num_pages < 16){
//         // cprintf("enqueue: va: %p\n", va);
//         p->psyc_pages++;
//     }
//     // exactly 16 pages in physical mem
//     else { 
//         page_out();
//         p->pages_array_in_physical[MAX_PSYC_PAGES - 1].page_address = va;
//     }
// }

int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  char *mem;
  uint a;
  #ifndef NONE
  struct proc *curproc = myproc();
  #endif
  if(newsz >= KERNBASE)
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
  #ifndef NONE
    // if (curproc->pid > 2) {
        int num_pages = curproc->psyc_pages;
        if(num_pages < MAX_PSYC_PAGES){
            #ifdef AQ
                if(curproc->pages_array_in_physical[FIRST].page_address == 0xffffffff){
                    curproc->pages_array_in_physical[FIRST].page_address = a;
                    curproc->pages_array_in_physical[FIRST].offset_in_swapfile = 0;
                    curproc->pages_array_in_physical[FIRST].ref_counter = 0;
                    curproc->psyc_pages++;
                }
                else{
                    int j = 0;
                    uint va = curproc->pages_array_in_physical[FIRST].page_address;
                    uint offset = curproc->pages_array_in_physical[FIRST].offset_in_swapfile;
                    uint refe_counter = curproc->pages_array_in_physical[FIRST].ref_counter;
                    for (j = 0; j < MAX_PSYC_PAGES; j++){
                        if(curproc->pages_array_in_physical[j].page_address == 0xffffffff){
                            curproc->pages_array_in_physical[j].page_address = va;
                            curproc->pages_array_in_physical[j].offset_in_swapfile = offset;
                            curproc->pages_array_in_physical[j].ref_counter = refe_counter;
                        }
                    }
                    curproc->pages_array_in_physical[FIRST].page_address = a;
                    curproc->pages_array_in_physical[FIRST].offset_in_swapfile = 0;
                    curproc->pages_array_in_physical[FIRST].ref_counter = 0;
                }
            #endif  
            #ifndef AQ
                if(curproc->pages_array_in_physical[num_pages].page_address  == 0xffffffff){
                    curproc->pages_array_in_physical[num_pages].page_address = a;
                    curproc->pages_array_in_physical[num_pages].offset_in_swapfile = 0;
                    curproc->pages_array_in_physical[num_pages].ref_counter = 0;
                    curproc->psyc_pages++;
                // cprintf("allocuvm: name: %s\n", curproc->name);
                // cprintf("allocuvm: proc name: %s - pid: %d - psyc_pages: %d\n", curproc->name, curproc->pid, curproc->psyc_pages);
                // cprintf("allocuvm: pages\n");
                // for (int i = 0; i < curproc->psyc_pages ; i++){
                //     cprintf("%d) %p\n", i, curproc->pages_array_in_physical[i].page_address);
                // }
            }
            else{
                cprintf("allocuvm: failed to update meta data\n");
            }
            #endif
            // for (int i = 0; i < curproc->psyc_pages ; i++){
            //         cprintf("%d) %p\n", i, curproc->pages_array_in_physical[i].page_address);
            //     }   
            
        }       
        else {
            cprintf("allocuvm: paging out\n");
            page_out();
            // cprintf("alocuvm: (curproc->q_head - 1) % MAX_PSYC_PAGES: %d\n", (curproc->q_head - 1) % MAX_PSYC_PAGES);
            curproc->pages_array_in_physical[curproc->avl_slot].page_address = a;
            curproc->pages_array_in_physical[curproc->avl_slot].offset_in_swapfile = 0;
            curproc->pages_array_in_physical[curproc->avl_slot].ref_counter = 0;
        }
    // }
    #endif
    mem = kalloc();
    // cprintf("allocuvm: a: %p\n", a);
    if(mem == 0){
      cprintf("allocuvm out of memory\n");
      deallocuvm(pgdir, newsz, oldsz);
      return 0;
    }
    memset(mem, 0, PGSIZE);
    if(mappages(pgdir, (char *)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
      cprintf("allocuvm out of memory (2)\n");
      deallocuvm(pgdir, newsz, oldsz);
      kfree(mem);
      return 0;
    }
  }
  return newsz;
}


// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  pte_t *pte;
  uint a, pa;
  // struct proc *curproc = myproc();
  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
    // if (curproc->pid > 2) {
    //     int i = 0;
    //     for (i = 0; i < MAX_PSYC_PAGES; i++){
    //         if(curproc->pages_array_in_physical[i].page_address == a){
    //             memset(&curproc->pages_array_in_physical[i].page_address, 0xffffffff, sizeof(struct page_to_offset));
    //             break;
    //         }
    //     }
    // }
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
      pa = PTE_ADDR(*pte);
      if(pa == 0)
        panic("kfree");
      char *v = P2V(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
}

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
    if(pgdir[i] & PTE_P){
      char * v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
}

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if(pte == 0)
    panic("clearpteu");
  *pte &= ~PTE_U;
}

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
      panic("copyuvm: pte should exist");
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
  }
  return d;

bad:
  freevm(d);
  return 0;
}

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if((*pte & PTE_P) == 0)
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}

// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
}

//PAGEBREAK!
// Blank page.
//PAGEBREAK!
// Blank page.
//PAGEBREAK!
// Blank page.


// Brings page from swapfile to physical mem
void
page_in(uint va){
    cprintf("page_in: start\n");
    // cprintf("page_in: va is: %p\n", va);
    struct proc *curproc = myproc();
    // curproc->psyc_pages++;
    // cprintf("page_in: start\n");
    int i = 0;
    for(i = 0 ; i < MAX_SWAPPED_PAGES ; i++){
        // cprintf("page_in: KUKURIKU1\n");
        if(curproc->pages_array_in_swapfile[i].page_address == PTE_ADDR(va)){
            int offset = curproc->pages_array_in_swapfile[i].offset_in_swapfile;
            char buffer[1024] = {0};
            // cprintf("page_in: KUKURIKU2\n");
			char *pa = kalloc();
            int j = 0;
            while(j < 4){
                if(readFromSwapFile(curproc, buffer, offset + j * 1024, PGSIZE / 4) == -1){
                    panic("page_in: Failed to read from swapFile");
                }
                memmove(pa + j * 1024, (void *)buffer, PGSIZE / 4);
                j++;
            }
            if(mappages(curproc->pgdir, (uint *) PTE_ADDR(va), PGSIZE, V2P(pa), PTE_U | PTE_W) < 0){
                panic("page_in: mappages failed");
            }
            
			// update meta data
			memset(&curproc->pages_array_in_swapfile[i], 0xffffffff, sizeof(struct page_to_offset));
			// #ifdef SCFIFO
            // int empty_index = (curproc->q_head - 1) % MAX_PSYC_PAGES;
			int empty_index = curproc->avl_slot;
            // cprintf("page_in: avl_slot in physical mem: %d\n", curproc->avl_slot);
            // cprintf("", curproc->pages_array_in_physical[empty_index].page_address;
            curproc->pages_array_in_physical[empty_index].page_address = PTE_ADDR(va);
            // cprintf("page_in: va paged in: %p\n", curproc->pages_array_in_physical[empty_index].page_address);
			curproc->pages_array_in_physical[empty_index].offset_in_swapfile = 0;
			curproc->pages_array_in_physical[empty_index].ref_counter = 0;
			curproc->swapped_pages--;
			// #endif

			cprintf("page_in: END\n");
            break;
        }
    }
    // update pages_array_in_swapfile
}

void nfua(){
    cprintf("nfua: start\n");
    struct proc *curproc = myproc();
    int i = 0; int min_index = 0;
    uint min = 0xffffffff;
    for(i = 0 ; i < MAX_PSYC_PAGES ; i++){
        if(curproc->pages_array_in_physical[i].page_address != 0xffffffff &&
         curproc->pages_array_in_physical[i].ref_counter <= min){
            min = curproc->pages_array_in_physical[i].ref_counter;
            min_index = i;
            // cprintf("nfua: new min ref counter is: %x\n", min);
            // cprintf("nfua: new min page_address: %p\n", curproc->pages_array_in_physical[i].page_address);
        }
    }
    // cprintf("nfua: min counter index: %d\n", min_index);
    pte_t *page = walkpgdir(curproc->pgdir, (uint *)curproc->pages_array_in_physical[min_index].page_address, 0);
    *page |= PTE_PG;
    *page &= ~PTE_P;
    char *pa = (char *)PTE_ADDR(P2V_WO(*page));
    char buffer[1024] = {0};
    int j = 0;
    for(i = 0 ; i < MAX_SWAPPED_PAGES ; i++){
        if (curproc->pages_array_in_swapfile[i].page_address == (uint)0xffffffff){
            for (j = 0; j < 4; j++){
                memmove(buffer, pa + j * (PGSIZE / 4), PGSIZE / 4);
                if(writeToSwapFile(curproc, buffer, (PGSIZE * i) + (j * (PGSIZE / 4)), PGSIZE / 4) == -1){
                    panic("nfua: can't write to swapfile");
                }
            }
            curproc->pages_array_in_swapfile[i].page_address = PTE_ADDR(curproc->pages_array_in_physical[min_index].page_address);
            curproc->pages_array_in_swapfile[i].offset_in_swapfile = PGSIZE * i;
            curproc->pages_array_in_swapfile[i].ref_counter = 0;
            memset(&curproc->pages_array_in_physical[min_index], 0xffffffff, sizeof(struct page_to_offset));
            // curproc->q_head = (curproc->q_head + 1) % MAX_PSYC_PAGES;
            curproc->avl_slot = min_index;
            curproc->swapped_pages++;
            break;
        }
    }
    kfree(pa);
    cprintf("nfua: end\n");
}

void lapa(){
    cprintf("lapa: start\n");
    struct proc *curproc = myproc();
    int i = 0;
    int min_index = 0;
    uint min = 0xffffffff;
	int min_num_ones = 32;
    for(i = 0 ; i < MAX_PSYC_PAGES ; i++){
        if(curproc->pages_array_in_physical[i].page_address != 0xffffffff){
            // uint va = curproc->pages_array_in_physical[i].page_address;
            uint cur_ref_counter = curproc->pages_array_in_physical[i].ref_counter;
            int cur_ones_amount = count_ones(cur_ref_counter);
            // cprintf("lapa: va: %p\n\trefcounter: %x\n\t one_amount: %d\n",va ,cur_ref_counter, cur_ones_amount);
	        if(min_num_ones >= cur_ones_amount){
	        	min_num_ones = cur_ones_amount;
	        	min = cur_ref_counter;
	            min_index = i;
	        	if(min > cur_ref_counter){
	            	min = cur_ref_counter;
		            min_index = i;
                }
                // cprintf("lapa: new min index is: %d\n", min_index);
                // cprintf("lapa: new min ref counter is: %x\n", min);
                // cprintf("lapa: new min page_address: %p\n", curproc->pages_array_in_physical[i].page_address);
            }
        }
    }
    // cprintf("lapa: **********************\n");
    // cprintf("lapa: min counter index chosen: %d\n", min_index);
    // cprintf("lapa: min counter chosen: %x\n", min);
    // cprintf("lapa: va paged out: %p\n", curproc->pages_array_in_physical[min_index].page_address);
    pte_t *page = walkpgdir(curproc->pgdir, (uint *)curproc->pages_array_in_physical[min_index].page_address, 0);
    *page |= PTE_PG;
    *page &= ~PTE_P;
    char *pa = (char *)PTE_ADDR(P2V_WO(*page));
    char buffer[1024] = {0};
    int j = 0;
    for(i = 0 ; i < MAX_SWAPPED_PAGES ; i++){
        if (curproc->pages_array_in_swapfile[i].page_address == (uint)0xffffffff){
            for (j = 0; j < 4; j++){
                memmove(buffer, pa + j * (PGSIZE / 4), PGSIZE / 4);
                if(writeToSwapFile(curproc, buffer, (PGSIZE * i) + (j * (PGSIZE / 4)), PGSIZE / 4) == -1){
                    panic("lapa: can't write to swapfile");
                }
            }
            curproc->pages_array_in_swapfile[i].page_address = PTE_ADDR(curproc->pages_array_in_physical[min_index].page_address);
            curproc->pages_array_in_swapfile[i].offset_in_swapfile = PGSIZE * i;
            curproc->pages_array_in_swapfile[i].ref_counter = 0;
            memset(&curproc->pages_array_in_physical[min_index], 0xffffffff, sizeof(struct page_to_offset));
            // curproc->q_head = (curproc->q_head + 1) % MAX_PSYC_PAGES;
            curproc->avl_slot = min_index;
            curproc->swapped_pages++;
            break;
        }
    }
    kfree(pa);
    cprintf("lapa: end\n");
}

void aq(){
    cprintf("aq: start\n");
    struct proc *curproc = myproc();
    	if (curproc->pages_array_in_physical[FIRST].page_address != 0xffffffff){
    		pte_t *page = walkpgdir(curproc->pgdir, (uint *)curproc->pages_array_in_physical[FIRST].page_address, 0);
    		*page |= PTE_PG;
            *page &= ~PTE_P;
            char *pa = (char *)PTE_ADDR(P2V_WO(*page));
          	char buffer[1024] = {0};
            int i = 0;
            for(i = 0 ; i < MAX_SWAPPED_PAGES ; i++){
               if (curproc->pages_array_in_swapfile[i].page_address == (uint)0xffffffff){
                   for (int j = 0; j < 4; j++){
            			memmove(buffer, pa + j * (PGSIZE / 4), PGSIZE / 4);
	                    if(writeToSwapFile(curproc, buffer, (PGSIZE * i) + (j * (PGSIZE / 4)), PGSIZE / 4) == -1){
	                        panic("aq: can't write to swapfile");
	                        }
                    }
                 	curproc->pages_array_in_swapfile[i].page_address = curproc->pages_array_in_physical[FIRST].page_address;
                	curproc->pages_array_in_swapfile[i].offset_in_swapfile = PGSIZE * i;
                    curproc->pages_array_in_swapfile[i].ref_counter = 0;
                    memset(&curproc->pages_array_in_physical[FIRST], 0xffffffff, sizeof(struct page_to_offset));
                    curproc->avl_slot = FIRST;
                   	// curproc->q_head = (curproc->q_head + 1) % MAX_PSYC_PAGES; //might be different/
                    curproc->swapped_pages++;
                       
                    break;   

    			}
    		}

		}
	cprintf("aq: end\n");
}

void scfifo(){
	cprintf("scfifo: in scfifo\n");
	struct proc *curproc = myproc();
	while (curproc->q_head < MAX_PSYC_PAGES){
		// cprintf("scfifo: in while loop\n");
		// cprintf("scfifo: psyc_pages: %d\n", curproc->psyc_pages);
		// cprintf("scfifo: swapped_pages: %d\n", curproc->swapped_pages);
	    if (curproc->pages_array_in_physical[curproc->q_head].page_address != (uint)0xffffffff){
			// cprintf("scfifo: found empty entry\n");
	        pte_t *page = walkpgdir(curproc->pgdir, (uint *)curproc->pages_array_in_physical[curproc->q_head].page_address, 0);
	        if ((*page & PTE_A) == PTE_A){
				// cprintf("scfifo: PTE_A is on\n");
	            *page &= ~PTE_A;
				curproc->q_head = (curproc->q_head + 1) % MAX_PSYC_PAGES;
	        }
	        else{
				// cprintf("scfifo: PTE_A is off\n");
	        	// cprintf("scfifo: found page not accessed: %p\n", curproc->pages_array_in_physical[curproc->q_head].page_address);
	        	// cprintf("scfifo: page: %p\n", page);
	        	// cprintf("scfifo: *page: %p\n", *page);
				*page |= PTE_PG;
                *page &= ~PTE_P;
        		char *pa = (char *)PTE_ADDR(P2V_WO(*page));
              	// cprintf("scfifo: pa: %p\n", pa);
              	// cprintf("scfifo: *pa: %d\n", *pa);
                // cprintf("scfifo: LOOK! LOOK HOW ASHKURY LOOK\n");
          		char buffer[1024] = {0};
                int i = 0;
                for(i = 0 ; i < MAX_SWAPPED_PAGES ; i++){
                    if (curproc->pages_array_in_swapfile[i].page_address == (uint)0xffffffff){
                        for (int j = 0; j < 4; j++){
            				memmove(buffer, pa + j * (PGSIZE / 4), PGSIZE / 4);
	                        if(writeToSwapFile(curproc, buffer, (PGSIZE * i) + (j * (PGSIZE / 4)), PGSIZE / 4) == -1){
	                            panic("scfifo: can't write to swapfile");
	                        }
                        }
                        curproc->pages_array_in_swapfile[i].page_address = PTE_ADDR(curproc->pages_array_in_physical[curproc->q_head].page_address);
                        curproc->pages_array_in_swapfile[i].offset_in_swapfile = PGSIZE * i;
                        curproc->pages_array_in_swapfile[i].ref_counter = 0;
                        memset(&curproc->pages_array_in_physical[curproc->q_head], 0xffffffff, sizeof(struct page_to_offset));
                        curproc->avl_slot = curproc->q_head;
                        curproc->q_head = (curproc->q_head + 1) % MAX_PSYC_PAGES;
                        curproc->swapped_pages++;
                        // cprintf("scfifo: look how BEN HA'NAAL\n");
                        break;
                    }
                }
            	kfree(pa);
                break;
	        }
	    }
    }
    cprintf("scfifo: END\n");
}



int
handle_page_fault(){
    struct proc *curproc = myproc();
    uint page_fault_address = rcr2();
// <<<<<<< HEAD
    // cprintf("handle_page_fault: address caused to page fault: 0x%p\n", PTE_ADDR(page_fault_address));
    // cprintf("handle_page_fault: number of physical pages: %d\n", curproc->psyc_pages);
    // cprintf("handle_page_fault: number of swapped pages: %d\n", curproc->swapped_pages);
    // print_pages();
    // pde_t *vaddr = &curproc->pgdir[PDX(page_fault_address)];
    // if (((int)(*vaddr) & PTE_P) != 0) {
    //  cprintf("handle_page_fault: PANDA0\n");
    //  if ((((uint*)PTE_ADDR(P2V(*vaddr)))[PTX(page_fault_address)] & PTE_PG) == PTE_PG) {
    //      cprintf("handle_page_fault: PANDA1\n");     
    //  }
    // }
    // cprintf("handle_page_fault: (uint*)PTE_ADDR(P2V(*vaddr)))[PTX(page_fault_address)]: %p\n", ((uint*)PTE_ADDR(P2V(*vaddr)))[PTX(page_fault_address)]);
// =======
   
// >>>>>>> 643375508c630d7007ecdf40195a437ca7c46e78
    pte_t *pte = walkpgdir(curproc->pgdir, (void *)page_fault_address, 0);
   
    if(curproc == 0){
        cprintf("handle_page_fault: Why the hell are we here?!\n");
        return 0;
    }
    if (curproc->pid <= 2) {
        cprintf("handle_page_fault: im init or shell. name: %s\n", curproc->name);
        return 0;
    }
    // cprintf("handle_page_fault: ((uint)*pte & PTE_PG): %d\n", ((uint)*pte & PTE_PG));
    if(((uint)*pte & PTE_PG) == PTE_PG){
        // cprintf("handle_page_fault: PANDA2\n");
        if(curproc->psyc_pages < MAX_PSYC_PAGES){
            cprintf("handle_page_fault: YOU SHOULDN'T BE HERE\n");
            page_in(PTE_ADDR(page_fault_address));
        }
        else{
            cprintf("handle_page_fault: paging out\n");
            page_out();
            curproc->number_page_fault++;
            cprintf("handle_page_fault: paging in\n");
            page_in(PTE_ADDR(page_fault_address));
        }
        return 1;
	}
    // cprintf("handle_page_fault: PANDA3\n");
    return 0;
}

// move page from physical mem to swapfile
void
page_out(){
	cprintf("page_out: Start\n");
    struct proc *curproc = myproc();
    #ifdef NFUA
    nfua();
    #endif

    #ifdef LAPA
    lapa();
    #endif

    #ifdef SCFIFO
    scfifo();
    #endif

    #ifdef AQ
    aq();
    #endif
    // curproc->psyc_pages--; // dont need that
    // choose a page to swap (policy dependent?)
    // call writetoswapfile
    // update pages_array_in_swapfile
    // free (using kfree?) a page in physical mem
    // update PTE_PG in pte
    // clear PTE_P in pte
    lcr3(V2P(curproc->pgdir));
    cprintf("page_out: END\n");
}

void update_AQ(){
	struct proc *curproc = myproc();
	int j = 0;
	int prev_accessed = 0;
	uint va = 0xffffffff;
	uint offset = 0xffffffff;
	//First iteration
	if(curproc->pages_array_in_physical[FIRST].page_address != 0xffffffff){
		va = curproc->pages_array_in_physical[FIRST].page_address;
		offset = curproc->pages_array_in_physical[FIRST].offset_in_swapfile;
		pte_t *pa = walkpgdir(curproc->pgdir, (void *)va, 0);
		if(*pa & PTE_A){
			prev_accessed = 1;
		}
	}
	//Rest of the iteration.
	for (j = 1; j < MAX_PSYC_PAGES;j++){
		if(curproc->pages_array_in_physical[j].page_address != 0xffffffff){
			uint va2 = curproc->pages_array_in_physical[j].page_address;
			// uint offset2 = curproc->pages_array_in_physical[j].offset_in_swapfile;
			pte_t *pa2 = walkpgdir(curproc->pgdir, (void *)va2, 0);
			if(*pa2 & PTE_A){
				//swapping cells in the array.
				if(!prev_accessed){
					curproc->pages_array_in_physical[j-1].page_address = va2;
					curproc->pages_array_in_physical[j-1].offset_in_swapfile = curproc->pages_array_in_physical[j].offset_in_swapfile;
					curproc->pages_array_in_physical[j].page_address = va;
					curproc->pages_array_in_physical[j].offset_in_swapfile = offset;
					prev_accessed = 1;
					va = va2;
					
    			}
    			else {
				prev_accessed = 0;
				va = curproc->pages_array_in_physical[j].page_address;
				offset = curproc->pages_array_in_physical[j].offset_in_swapfile;
		      	}
            }
		}
	}
}



void update_ref_counter(){
    // cprintf("update_ref_counter: start\n");
    struct proc *curproc = myproc();
    uint msb = 0x80000000; 
    for (int i = 0 ; i < MAX_PSYC_PAGES ; i++){
        if(curproc->pages_array_in_physical[i].page_address != 0xffffffff){
        	// cprintf("update_ref_counter: va entry: %p\n\told ref counter: %x\n", curproc->pages_array_in_physical[i].page_address, curproc->pages_array_in_physical[i].ref_counter);
            curproc->pages_array_in_physical[i].ref_counter >>= 1;
            uint va = curproc->pages_array_in_physical[i].page_address;
            pte_t *pa = walkpgdir(curproc->pgdir, (void *)va, 0);
            if(*pa & PTE_A){
                curproc->pages_array_in_physical[i].ref_counter += msb;
            }
        }
    	// cprintf("update_ref_counter: *va entry: %p\n\tnew ref counter: %x\n", curproc->pages_array_in_physical[i].page_address, curproc->pages_array_in_physical[i].ref_counter);
    }
    // cprintf("update_ref_counter: END\n");
}


void print_pages(){
    struct proc *curproc = myproc();
    int i = 0;
    for (i = 0; i < MAX_PSYC_PAGES; i++){
        cprintf("print_pages: %d.ph) %p\n", i,curproc->pages_array_in_physical[i].page_address);
    }
    for (i = 0; i < MAX_PSYC_PAGES; i++){
        cprintf("print_pages: %d.sw) %p\n", i,curproc->pages_array_in_swapfile[i].page_address);
    }
}